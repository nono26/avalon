﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Item Name="Poste de travail" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">Poste de travail/VI Serveur</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">Poste de travail/VI Serveur</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="crc16-ccitt.vi" Type="VI" URL="../crc16-ccitt.vi"/>
		<Item Name="envoi_message_V7.vi" Type="VI" URL="../envoi_message_V7.vi"/>
		<Item Name="inversion_byte.vi" Type="VI" URL="../inversion_byte.vi"/>
		<Item Name="miseEnForme.vi" Type="VI" URL="../miseEnForme.vi"/>
		<Item Name="reception_donnees_epos.vi" Type="VI" URL="../reception_donnees_epos.vi"/>
		<Item Name="Dépendances" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
			</Item>
			<Item Name="CRC-16-CCITT-xMODEM.vi" Type="VI" URL="../../projet/commande_moteur/Inline CRC/CRC SubVIs/Wrappers/CRC-16-CCITT-xMODEM.vi"/>
			<Item Name="Inline CRC-16-CCITT.vi" Type="VI" URL="../../projet/commande_moteur/Inline CRC/CRC SubVIs/Inline CRC-16-CCITT.vi"/>
		</Item>
		<Item Name="Spécifications de construction" Type="Build"/>
	</Item>
	<Item Name="NI-GenericDesktopPC-11190C33" Type="RT Desktop">
		<Property Name="alias.name" Type="Str">NI-GenericDesktopPC-11190C33</Property>
		<Property Name="alias.value" Type="Str">192.168.1.210</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,PharLap;CPU,x86;</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">3</Property>
		<Property Name="host.TargetOSID" Type="UInt">15</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
Listen 8000
ThreadLimit 10
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
LoadModulePath $LVSERVER_MODULEPATHS
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule
LoadModule dir libdirModule
LoadModule copy libcopyModule

AddHandler LVAuthHandler
AddHandler LVRFPHandler

AddHandler dirHandler
AddHandler copyHandler 

KeepAlive on
KeepAliveTimeout 60
Timeout 60

</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="relative_pos_avalon.vi" Type="VI" URL="../relative_pos_avalon.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/Close.vi"/>
				<Item Name="DisableAxis.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/DisableAxis.vi"/>
				<Item Name="EnableAxis.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/EnableAxis.vi"/>
				<Item Name="EposCmd.dll" Type="Document" URL="/&lt;instrlib&gt;/maxon EPOS/EposCmd.dll"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Converter (ErrCode or Status).vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/Error Converter (ErrCode or Status).vi"/>
				<Item Name="GetAxis.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/GetAxis.vi"/>
				<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/Initialize.vi"/>
				<Item Name="MoveToRelativePosition.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/MoveToRelativePosition.vi"/>
				<Item Name="VCS Activate Profile Position Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Activate Profile Position Mode.vi"/>
				<Item Name="VCS Clear Fault.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Clear Fault.vi"/>
				<Item Name="VCS Close All Devices.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Close All Devices.vi"/>
				<Item Name="VCS Move To Position.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Move To Position.vi"/>
				<Item Name="VCS Open Device.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Open Device.vi"/>
				<Item Name="VCS Set Disable State.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Set Disable State.vi"/>
				<Item Name="VCS Set Enable State.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Set Enable State.vi"/>
				<Item Name="VCS Set Operation Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Set Operation Mode.vi"/>
				<Item Name="VCS Set Position Profile.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Set Position Profile.vi"/>
				<Item Name="VCS Set Protocol Stack Settings.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Set Protocol Stack Settings.vi"/>
				<Item Name="VCS Wait For Target Reached.vi" Type="VI" URL="/&lt;instrlib&gt;/maxon EPOS/EPOSLibrary.llb/VCS Wait For Target Reached.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
